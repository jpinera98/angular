export class Cita{
    idCita: String;
    fechaHora: String;
    motivoCita: String;
    medicoIdUsuario: String;
    pacienteIdUsuario: String;
    diagnosticoIdDiagnostico: String;

    constructor(){};
}