import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {ServiceService} from '../../Service/service.service'
import {Paciente} from 'src/app/Modelo/Paciente'

@Component({
  selector: 'app-listar-paciente',
  templateUrl: './listar-paciente.component.html',
  styleUrls: ['./listar-paciente.component.css']
})
export class ListarPacienteComponent implements OnInit {


  pacientes:Paciente[];
  constructor(private service:ServiceService, private router:Router) { }

  ngOnInit(): void {
    this.service.getPacientes()
    .subscribe(data=>{
      this.pacientes=data;
    })
  }

  Delete(paciente:Paciente){
    this.service.deletePaciente(paciente)
    .subscribe(data=>{
      this.pacientes=this.pacientes.filter(p=>p!=paciente);
      alert("Paciente eliminado");
    })
  }

}
