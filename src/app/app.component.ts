import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ProyectoCitasMedicas';

  constructor(private router:Router){}

  Listar(){
    this.router.navigate(["listar"]);
  }
  Nuevo(){
    this.router.navigate(["add"]);
  }
  ConsultaCitas(){
    this.router.navigate(["listarCitas"]);
  }
  NuevaCita(){
    this.router.navigate(["addCita"])
  }

  ListarPacientes(){
    this.router.navigate(["listarPacientes"]);
  }
}
