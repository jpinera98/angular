import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http'
import { Medico } from '../Modelo/Medico';
import { Cita } from '../Modelo/Cita';
import { Diagnostico } from '../Modelo/Diagnostico'
import { environment } from '@env/environment'
import { Observable } from 'rxjs';
import { Paciente } from '@app/Modelo/Paciente';



@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor(private http:HttpClient) { }


  private header = new Headers({'Content-Type':'application/json'});
  UrlM='http://localhost:8080/medicos'
  UrlC='http://localhost:8080/citas'
  UrlD='http://localhost:8080/diagnosticos'
  UrlP='http://localhost:8080/pacientes'

  createCita(cita:Cita){
    //let iJson = JSON.stringify(cita);
    //return this.http.post(this.UrlC, iJson)

      return this.http.post<Cita>(this.UrlC, cita);
  }

  updateCita(cita:Cita){
    return this.http.put<Cita>(this.UrlC, cita);
  }

  getMedicos(){

    return this.http.get<Medico[]>(this.UrlM);
  }

  getCitas(){

    return this.http.get<Cita[]>(this.UrlC);
  }

  getPacientes(){
    return this.http.get<Paciente[]>(this.UrlP);
  }

  createMedico(medico:Medico){
    return this.http.post<Medico>(this.UrlM,medico);
  }

  deleteCita(cita:Cita){
    return this.http.delete<Cita>(this.UrlC+"/"+cita.idCita);
  }

  deleteMedico(medico:Medico){
    return this.http.delete<Cita>(this.UrlC+"/"+medico.idUsuario);
  }

  deletePaciente(paciente:Paciente){
    return this.http.delete<Paciente>(this.UrlP+"/"+paciente.idUsuario);
  }


  registerMedico(medico:Medico){
    return this.http.post<Medico>(this.UrlM,medico);
  }

  registerPaciente(paciente:Paciente){
    return this.http.post<Paciente>(this.UrlP, paciente);
  }

  getDiagnostico(){
    return this.http.get<Diagnostico[]>(this.UrlD);
  }

  loginPaciente(paciente:Paciente){
    return this.http.post<Paciente>(this.UrlP+"/login", paciente);
  }

}
