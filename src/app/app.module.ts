import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListarComponent } from './Medico/listar/listar.component';
import { AddComponent } from './Medico/add/add.component';
import { EditComponent } from './Medico/edit/edit.component';
import { FormsModule } from '@angular/forms';
import { ServiceService } from '../app/Service/service.service';
import { HttpClientModule } from '@angular/common/http';
import { ListarCitaComponent } from './Cita/listar-cita/listar-cita.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './shared/components/header/header.component';
import { FooterComponent } from './shared/components/footer/footer.component';
import { MaterialModule } from '@app/material.module';
import { SidebarModule } from './shared/components/sidebar/sidebar.module';
import { AddCitaComponent } from './Cita/add-cita/add-cita.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RegisterComponent} from './pages/register/register.component';
import {ListarPacienteComponent} from './Paciente/listar-paciente/listar-paciente.component';




@NgModule({
  declarations: [
    AppComponent,
    ListarComponent,
    AddComponent,
    EditComponent,
    ListarCitaComponent,
    AddCitaComponent,
    HeaderComponent,
    FooterComponent,
    RegisterComponent,
    ListarPacienteComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialModule,
    SidebarModule,
    ReactiveFormsModule
  ],
  providers: [ServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
