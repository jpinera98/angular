import { Component, OnInit } from '@angular/core';
import { FormGroup, NgForm, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Cita } from '@app/Modelo/Cita';
import { ServiceService } from '@app/Service/service.service';


@Component({
  selector: 'app-add-cita',
  templateUrl: './add-cita.component.html',
  styleUrls: ['./add-cita.component.css'],
  providers: [ServiceService]
})
export class AddCitaComponent implements OnInit {

  constructor(private router:Router, 
              private service:ServiceService,
              private fb : FormBuilder) { this.crearControles();}
  
  titulo = "Concertar una cita";
  
  public modelCita:Cita;
  form: FormGroup;
 
  ngOnInit(): void {
  }

  crearControles(){
    this.form = this.fb.group({
      motivoCita: '',
      medicoIdUsuario: '',
      pacienteIdUsuario: '',
      fechaHora: ''
    })
  }

  guardarCita(){
    this.service.createCita(this.form.value)
                .subscribe(
                  rt => console.log(rt),
                  er => console.log(er),
                  () => console.log('Terminado')
                );
    this.router.navigate(["listarCitas"]);
  }

}
