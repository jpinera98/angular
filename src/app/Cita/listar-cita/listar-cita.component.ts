import { Component, OnInit } from '@angular/core';
import { Form, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Diagnostico } from '@app/Modelo/Diagnostico';
import { Cita } from 'src/app/Modelo/Cita'
import { ServiceService } from 'src/app/Service/service.service';

@Component({
  selector: 'app-listar-cita',
  templateUrl: './listar-cita.component.html',
  styleUrls: ['./listar-cita.component.css']
})
export class ListarCitaComponent implements OnInit {

  citas:Cita[];
  diagnosticos:Diagnostico[];
  diagnosticoActual:Diagnostico;
  citaActual:Cita;
  numDiagnostico:Number;
  form:FormGroup;
  flag:boolean;


  constructor(private service:ServiceService,
      private router:Router,
      private fb : FormBuilder) { this.crearControles();}

  ngOnInit(): void {
    this.service.getCitas()
    .subscribe(data=>{
      this.citas=data;
    })
    this.service.getDiagnostico()
    .subscribe(data => {
      this.diagnosticos = data;
    });
  }

  crearControles(){
    this.form = this.fb.group({
      idCita: '',
      motivoCita: '',
      fechaHora: '',
      medicoIdUsuario: '',
      pacienteIdUsuario: '',
      diagnosticoIdDiagnostico: ''
    });
  }

  Delete(cita:Cita){
    this.service.deleteCita(cita)
    .subscribe(data=>{
      this.citas=this.citas.filter(c=>c!==cita);
      alert("Cita eliminada");
    })
  }

  Update(){
    this.service.updateCita(this.form.value)
                .subscribe(
                  rt => console.log(rt),
                  er => console.log(er),
                  () => console.log('Terminado')
                );
    location.reload();
  }

  mostrarDiagnostico(id:String):void{
    this.flag = true;
    this.diagnosticoActual = this.diagnosticos.find(d => d.idDiagnostico == id);
  }

}
