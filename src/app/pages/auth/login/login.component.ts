import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Medico } from 'src/app/Modelo/Medico';
import { ServiceService } from 'src/app/Service/service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [ServiceService]
})
export class LoginComponent implements OnInit {




  constructor(private router:Router,
              private service:ServiceService,
              private fb: FormBuilder) { this.crearControles()}


  titulo = "Login";
  form: FormGroup;

  crearControles(){
    this.form = this.fb.group({
      usuario: '',
      clave: ''
    })
  }


  ngOnInit(): void {
  }


  onLogin(){
    this.service.loginPaciente(this.form.value)
                .subscribe(data => {
                  this.router.navigate(["listarCitas"]);
                },
                error => {
                  alert("Usuario o contraseña incorrectos");
                })
    
  }

}
