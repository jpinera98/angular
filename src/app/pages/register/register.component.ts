import { Component, OnInit } from '@angular/core';
import { FormGroup, NgForm, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Paciente } from '@app/Modelo/Paciente';
import { ServiceService } from '@app/Service/service.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers : [ServiceService]
})
export class RegisterComponent implements OnInit {

  constructor(private router:Router,
              private service:ServiceService,
              private fb:FormBuilder) { this.crearControles(); }

  ngOnInit(): void {
  }

  public modelPaciente:Paciente;
  form:FormGroup;

  crearControles(){
    this.form = this.fb.group({
      nombre : '',
      apellidos : '',
      usuario : '',
      clave : '',
      nss : '',
      numTarjeta : '',
      telefono: '',
      direccion : ''
    })
  }

  guardarPaciente(){
    this.service.registerPaciente(this.form.value)
                      .subscribe(
                        rt => console.log(rt),
                        er => console.log(er),
                        () => console.log('Terminado')
                      );
    this.router.navigate(["login"]);
  }

}
