import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddCitaComponent } from './Cita/add-cita/add-cita.component';
import { ListarCitaComponent } from './Cita/listar-cita/listar-cita.component';
import { AddComponent } from './Medico/add/add.component';
import { EditComponent } from './Medico/edit/edit.component';
import { ListarComponent } from './Medico/listar/listar.component';
import { ListarPacienteComponent } from './Paciente/listar-paciente/listar-paciente.component';
import { RegisterComponent } from './pages/register/register.component';



const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: 'listar', component:ListarComponent},
  {path: 'add', component:AddComponent},
  {path:  'edit', component:EditComponent},
  {path: 'listarCitas', component:ListarCitaComponent},
  {path: 'addCita', component: AddCitaComponent},
  {path: 'listarPacientes', component: ListarPacienteComponent},
  { path: 'home', loadChildren: () => import('./pages/home/home.module').then(m => m.HomeModule) },
  { path: 'notFound', loadChildren: () => import('./pages/not-found/not-found.module').then(m => m.NotFoundModule) },
  { path: 'admin', loadChildren: () => import('./pages/admin/admin.module').then(m => m.AdminModule) },
  { path: 'login', loadChildren: () => import('./pages/auth/login/login.module').then(m => m.LoginModule) },
  { path: 'register', component: RegisterComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
