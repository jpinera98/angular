import { Component, OnInit } from '@angular/core';
import { ServiceService } from 'src/app/Service/service.service';
import { Router } from '@angular/router';
import { Medico } from 'src/app/Modelo/Medico'


@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  modelMedico = new Medico();
  constructor(private router:Router, private service:ServiceService) { }

  ngOnInit(): void {
  }

  Guardar(medico:Medico){
    this.service.createMedico(medico)
    .subscribe(data=>{
      alert("Se registro adecuadamente");
      this.router.navigate(["listar"]);
    })
  }

}
